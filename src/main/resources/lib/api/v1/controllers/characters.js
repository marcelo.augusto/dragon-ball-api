const ContentLib = require('/lib/xp/content');
const ContextLib = require('/lib/xp/context');
const PortalLib = require('/lib/xp/portal');

const Utils = require('/lib/api/utils');

exports.get = function (request) {
	const resources = Utils.extractResourceFromPath({ request, resource: '/api/v1/characters' })

	return ContextLib.run(Utils.AuthContext, function () {
		if (resources.splittedPath.length === 3) {
			const response = fetchCharacters(request.params)
	
			return {
				status: (response && response.total) ? 200 : 204,
				body: response.data,
				contentType: 'application/json',
				headers: {
					'X-Total-Count': response.total
				}
			}
		}
	
		if (Utils.isValidUUID(resources.id)) {
			const response = fetchCharacterById({ id: resources.id, loadMovies: resources.shouldLoadMovies })
	
			return {
				status: (response && response.id) ? 200 : 404,
				contentType: 'application/json',
				body: (response && response.id) ? response : { message: 'Character not found' }
			}
		} else {
			return {
				contentType: 'application/json',
				status: 400,
				body: {
					message: 'Invalid ID supplied'
				}
			}
		}
	})

}

/**
 * Fetch characters content type based on query params
 * @param {Object} params
 * @param {String} params.name
 * @param {number} params.offset
 * @param {number} [params.limit=20] Number of items to return. Valid range are 1 to 50. Default = 20
 * @param {'modifiedTime'|'displayName'} [params.sortBy=modifiedTime] The field to sort by.
 * @param {'asc'|'desc'} params.sortOrder Sort the field in Ascending or Descending order.
 */
function fetchCharacters (params) {
	const query = Utils.buildQuery({ name: params.name })
	const start = params.offset || 0
	const count = Utils.getSanitizedLimitParam(params.limit)
	const sort = Utils.getSanitizedSortParam({ sort: params && params.sortBy, order: params && params.sortOrder })

  const response = ContentLib.query({
		query,
		start,
		count,
		sort: sort.fullStatement,
		contentTypes: [`${app.name}:character`]
	})
	
	return {
		total: response.total,
		data: response.hits.map(item => ({
			id: item._id,
			name: item.data.name,
			race: item.data.race,
			image: PortalLib.imageUrl({ id: item.data.image, scale: '(1,1)', type: 'absolute' }),
		}))
	}
}

/**
 * 
 * @param {Object} params 
 * @param {String} params.id
 * @param {Boolean} [params.loadMovies] if true, the movies associated with the character will be loaded
 */
function fetchCharacterById (params) {
	if (!params || !params.id) return

	let response = ContentLib.query({
		query: `_id = '${params.id}'`,
		count: 1,
		contentTypes: [`${app.name}:character`]
	})

	if (!response.total) return

	const character = response.hits[0]
	let movies

	if (params.loadMovies) {
		response = ContentLib.query({
			query: `data.characters IN ('${character._id}')`,
			count: -1,
			contentTypes: [`${app.name}:movie`]
		})

		movies = response.hits.map(item => ({
			id: item._id,
			title: item.data.title,
			release_date: item.data.release_date,
			poster: PortalLib.imageUrl({ id: item.data.poster, scale: '(1,1)', type: 'absolute' })
		}))
	}

	const xData = getCharacterXData(character.x)

	return {
		id: character._id,
		name: character.data.name,
		description: character.data.description,
		image: PortalLib.imageUrl({ id: character.data.image, scale: '(1,1)', type: 'absolute' }),
		race: character.data.race,
		transformations: Utils.forceArray(character.data.transformations).map(item => ({
			name: item.name,
			image: PortalLib.imageUrl({ id: item.image, scale: '(1,1)', type: 'absolute' })
		})),
		universe: xData.universe,
		movies
	}
}

function getCharacterXData (x) {
	const xDataAppName = Utils.getXData()
	const xData = x && x[xDataAppName] && x[xDataAppName]['character-extra-data']

	if (!xData) return

	return {
		universe: ContentLib.get({ key: xData.universe }).data.name
	}
}