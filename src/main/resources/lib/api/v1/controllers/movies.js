const ContentLib = require('/lib/xp/content');
const ContextLib = require('/lib/xp/context');
const PortalLib = require('/lib/xp/portal');

const Utils = require('/lib/api/utils');

exports.get = function (request) {
	const resources = Utils.extractResourceFromPath({ request, resource: '/api/v1/movies' })

	return ContextLib.run(Utils.AuthContext, function () {
		if (resources.splittedPath.length === 3) {
			const response = fetchMovies(request.params)
	
			return {
				status: (response && response.total) ? 200 : 204,
				body: response.data,
				contentType: 'application/json',
				headers: {
					'X-Total-Count': response.total
				}
			}
		}
	
		if (Utils.isValidUUID(resources.id)) {
			const response = fetchMovieById({ id: resources.id, loadMovies: resources.shouldLoadMovies })
	
			return {
				status: (response && response.id) ? 200 : 404,
				contentType: 'application/json',
				body: (response && response.id) ? response : { message: 'Movie not found' }
			}
		} else {
			return {
				contentType: 'application/json',
				status: 400,
				body: {
					message: 'Invalid ID supplied'
				}
			}
		}
	})

}

/**
 * Fetch movies content type based on query params
 * @param {Object} params
 * @param {String} params.name
 * @param {number} params.offset
 * @param {number} [params.limit=20] Number of items to return. Valid range are 1 to 50. Default = 20
 * @param {'modifiedTime'|'displayName'} [params.sortBy=modifiedTime] The field to sort by.
 * @param {'asc'|'desc'} params.sortOrder Sort the field in Ascending or Descending order.
 */
function fetchMovies (params) {
	const query = Utils.buildQuery({ name: params.name })
	const start = params.offset || 0
	const count = Utils.getSanitizedLimitParam(params.limit)
	const sort = Utils.getSanitizedSortParam({ sort: params && params.sortBy, order: params && params.sortOrder })

  const response = ContentLib.query({
		query,
		start,
		count,
		sort: sort.fullStatement,
		contentTypes: [`${app.name}:movie`]
	})
	
	return {
		total: response.total,
		data: response.hits.map(item => ({
			id: item._id,
			title: item.data.title,
			overview: item.data.overview,
			poster: PortalLib.imageUrl({ id: item.data.poster, scale: '(1,1)', type: 'absolute' }),
      release_date: item.data.release_date
		}))
	}
}

/**
 * 
 * @param {Object} params 
 * @param {String} params.id
 * @param {Boolean} [params.loadMovies] if true, the movies associated with the character will be loaded
 */
function fetchMovieById (params) {
	if (!params || !params.id) return

	let response = ContentLib.query({
		query: `_id = '${params.id}'`,
		count: 1,
		contentTypes: [`${app.name}:movie`]
	})

	if (!response.total) return

	const movie = response.hits[0]

	return {
		id: movie._id,
    title: movie.data.title,
    overview: movie.data.overview,
    poster: PortalLib.imageUrl({ id: movie.data.poster, scale: '(1,1)', type: 'absolute' }),
    release_date: movie.data.release_date,
    characters: Utils.forceArray(movie.data.characters).map(id => {
      const character = ContentLib.get({ key: id })

      return {
        id: character._id,
        name: character.data.name,
        race: character.data.race,
        image: PortalLib.imageUrl({ id: character.data.image, scale: '(1,1)', type: 'absolute' }), 
      }
    })
	}
}