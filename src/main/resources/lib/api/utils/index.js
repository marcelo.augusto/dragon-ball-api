const AuthContext = {
	repository: 'com.enonic.cms.default',
	branch: 'master',
	user: {
		login: 'su',
		idProvider: 'system'
	},
	principals: ['role:system.admin'],
	attributes: {
		ignorePublishTimes: true
	}
}

/**
 * 
 * @param {Object} params 
 * @param {Object} params.request
 * @param {String} params.resource
 */
const extractResourceFromPath = (params) => {
  const fullPath = params.request && params.request.path
	const resourceIndex = fullPath.indexOf(params.resource)
	const path = fullPath.slice(resourceIndex)
	const splittedPath = (path && path.split('/').filter(Boolean)) || []
	const id = splittedPath.filter(resource => isValidUUID(resource))[0]
  const shouldLoadMovies = splittedPath.some(resource => resource === 'movies')

  return {
    id,
    shouldLoadMovies,
    splittedPath,
  }
}

const isValidUUID = (str) => {
	return str.match(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/g)
}

const forceArray = (value) => {
  if (!value) return []

  if (Array.isArray(value)) return value

  return [value]
}

const getXData = () => String(app.name).split('.').join('-')

/**
 * Build a query statement based on query params
 * @param {Object} params
 * @param {String} params.name character name attribute
 * @returns {String} the query statement or an empty string if the platform value is invalid
 */
const buildQuery = (params) => {
	if (params && params.name) {
		return `data.name LIKE '*${params.name}*'`
	}
	return ''
}

/**
 * Sanitizes the sortBy and orderBy query params and return a mapped object with the expected values
 * @param {Object} params
 * @param {String} params.sort sortBy value from query param. Valid values are 'modifiedTime' or 'displayName'. Default is 'modifiedTime'
 * @param {String} params.order sortOrder value from query param. Valid values are 'asc' or 'desc'. Default is 'desc'
 * @returns {{ sortBy, sortOrder, fullStatement }}
 */
const getSanitizedSortParam = (params) => {
	let sort = params.sort || 'modifiedTime'
	let order = params.order || 'desc'

	if (!['modifiedTime', 'displayName'].some(item => item.toLowerCase() === sort.toLowerCase())) {
		sort = 'modifiedTime'
	}

	if (!['asc', 'desc'].some(item => item.toLowerCase() === order.toLowerCase())) {
		order = 'desc'
	}

	return {
		sortBy: sort,
		sortOrder: order,
		fullStatement: `${sort} ${order.toUpperCase()}`
	}
}

/**
 * Sanitizes the limit checking if the number is between the valid range
 * @param {Number} limit amount of items to be returned. Valid range is numbers between 1 and 50. Default is 10.
 * @returns {Number} the informed param if it is valid or default 10.
 */
const getSanitizedLimitParam = (limit) => {
	if (!limit) return 10
	if (limit < 1 || limit > 50) return 10

	return limit
}

module.exports = {
  buildQuery,
  extractResourceFromPath,
  forceArray,
  getSanitizedSortParam,
  getSanitizedLimitParam,
	getXData,
  isValidUUID,
  AuthContext
}